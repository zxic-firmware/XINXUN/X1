#!/bin/sh

last_net_conn_flg=-1
net_conn_flg=0
curr_ping_interval=0
wan_conn_flg=0
wan_input_flg=0

ping_single_addr()
{
    local pkt_num=1
    local timeout_val=1
    local max_try_cnt=1
    local cnt=0

    while [ $cnt -lt $max_try_cnt ]
    do
        ping -c $pkt_num -W $timeout_val $1
        if [ 0 == $? ]; then
            net_conn_flg=1
            break
        fi

        cnt=`expr $cnt + 1`
        curr_ping_interval=`expr $curr_ping_interval + 1`
    done
}

ping_chk()
{
    local pri_dns_addr=`nv get wan1_pridns`
    local dns_addr1=210.2.4.8
    local dns_addr2=114.114.114.114
    local dns_addr3=8.8.8.8

    net_conn_flg=0
    curr_ping_interval=0

    if [ "${pri_dns_addr}" == "0.0.0.0" ]; then
        pri_dns_addr=14.119.104.189
    fi

    for args in $pri_dns_addr $dns_addr1 $dns_addr2 $dns_addr3
    do
        echo try $args
        ping_single_addr $args

        if [ 1 == $net_conn_flg ]; then
            break
        fi
    done
}

main()
{
    while [ 1 ]
    do
        wan_state=$(nv get rj45_state)
        if [ "${wan_state}" == "working" -a "${wan_input_flg}" == "0" ]; then
            led_ctrl_cmd "wan_in"
            wan_input_flg=1
        fi

        if [ "${wan_state}" != "working" -a "${wan_input_flg}" == "1" ]; then
            led_ctrl_cmd "wan_out"
            wan_input_flg=0
        fi

        wan_mode=$(nv get blc_wan_mode)
        ppp_state=$(nv get ppp_status)
        if [ "${wan_mode}" == "PPP" -o "${ppp_state}" != "ppp_disconnected" ]; then
            if [ "${wan_conn_flg}" == "1" ]; then
                nv set fl_pppoe_status=disconnected
                led_ctrl_cmd "wan_disconnect"
                wan_conn_flg=0
                last_net_conn_flg=-1
            fi
            sleep 5
            continue
        fi

        ping_chk

        if [ $net_conn_flg -ne $last_net_conn_flg ]; then
            echo wan conn state changed to $net_conn_flg
            last_net_conn_flg=$net_conn_flg

            if [ 1 == $net_conn_flg ]; then
                nv set fl_pppoe_status=connected
                led_ctrl_cmd "wan_connect"
                wan_conn_flg=1
            else
                nv set fl_pppoe_status=disconnected
                led_ctrl_cmd "wan_disconnect"
                wan_conn_flg=0
            fi
        fi
        sleep 5
    done
}

main
