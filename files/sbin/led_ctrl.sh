#!/bin/sh

batt_led_blink()
{

    usb=`cat /sys/class/power_supply/charger/online` 
    n_vlotage=`sed 's/mV//g' /sys/class/power_supply/battery/voltage_now` 

    if [ "${usb}" == "0" ]; then
        echo 0 > /sys/class/leds/batt1_led/brightness
        echo 0 > /sys/class/leds/batt2_led/brightness
        echo 0 > /sys/class/leds/batt3_led/brightness
        if [ "${n_vlotage}" \> "4066" ]; then
            echo timer > /sys/devices/platform/leds-gpio.1/leds/batt3_led/trigger
            echo 500 > /sys/devices/platform/leds-gpio.1/leds/batt3_led/delay_on
            echo 500 > /sys/devices/platform/leds-gpio.1/leds/batt3_led/delay_off
            sleep 5

            echo none > /sys/devices/platform/leds-gpio.1/leds/batt3_led/trigger
            echo 0 > /sys/devices/platform/leds-gpio.1/leds/batt3_led/brightness
        else
            if [ "${n_vlotage}" \> "3644" ]; then
                echo timer > /sys/devices/platform/leds-gpio.1/leds/batt2_led/trigger
                echo 500 > /sys/devices/platform/leds-gpio.1/leds/batt2_led/delay_on
                echo 500 > /sys/devices/platform/leds-gpio.1/leds/batt2_led/delay_off
                sleep 5

                echo none > /sys/devices/platform/leds-gpio.1/leds/batt2_led/trigger
                echo 0 > /sys/devices/platform/leds-gpio.1/leds/batt2_led/brightness
            else
                echo timer > /sys/devices/platform/leds-gpio.1/leds/batt1_led/trigger
                echo 500 > /sys/devices/platform/leds-gpio.1/leds/batt1_led/delay_on
                echo 500 > /sys/devices/platform/leds-gpio.1/leds/batt1_led/delay_off
                sleep 5

                echo none > /sys/devices/platform/leds-gpio.1/leds/batt1_led/trigger
                echo 0 > /sys/devices/platform/leds-gpio.1/leds/batt1_led/brightness
            fi
        fi
        echo 0 > /sys/class/leds/batt1_led/brightness
        echo 0 > /sys/class/leds/batt2_led/brightness
        echo 0 > /sys/class/leds/batt3_led/brightness
    fi
}

batt_led_control()
{
    usb=`cat /sys/class/power_supply/charger/online` 
    n_vlotage=`sed 's/mV//g' /sys/class/power_supply/battery/voltage_now` 
    if [ "${usb}" == "0" ]; then
        if [ "${n_vlotage}" \> "4066" ]; then
            echo timer > /sys/devices/platform/leds-gpio.1/leds/batt3_led/trigger
            echo 500 > /sys/devices/platform/leds-gpio.1/leds/batt3_led/delay_on
            echo 500 > /sys/devices/platform/leds-gpio.1/leds/batt3_led/delay_off
            sleep 5

            echo none > /sys/devices/platform/leds-gpio.1/leds/batt3_led/trigger
            echo 0 > /sys/devices/platform/leds-gpio.1/leds/batt3_led/brightness
        else
            if [ "${n_vlotage}" \> "3644" ]; then
                echo timer > /sys/devices/platform/leds-gpio.1/leds/batt2_led/trigger
                echo 500 > /sys/devices/platform/leds-gpio.1/leds/batt2_led/delay_on
                echo 500 > /sys/devices/platform/leds-gpio.1/leds/batt2_led/delay_off
                sleep 5

                echo none > /sys/devices/platform/leds-gpio.1/leds/batt2_led/trigger
                echo 0 > /sys/devices/platform/leds-gpio.1/leds/batt2_led/brightness
            else
                echo timer > /sys/devices/platform/leds-gpio.1/leds/batt1_led/trigger
                echo 500 > /sys/devices/platform/leds-gpio.1/leds/batt1_led/delay_on
                echo 500 > /sys/devices/platform/leds-gpio.1/leds/batt1_led/delay_off
                sleep 5

                echo none > /sys/devices/platform/leds-gpio.1/leds/batt1_led/trigger
                echo 0 > /sys/devices/platform/leds-gpio.1/leds/batt1_led/brightness
            fi
        fi
    else
        if [ "${n_vlotage}" \> "4090" ]; then
            echo timer > /sys/devices/platform/leds-gpio.1/leds/batt3_led/trigger
            echo 500 > /sys/devices/platform/leds-gpio.1/leds/batt3_led/delay_on
            echo 500 > /sys/devices/platform/leds-gpio.1/leds/batt3_led/delay_off
            sleep 5

            echo none > /sys/devices/platform/leds-gpio.1/leds/batt3_led/trigger
            echo 0 > /sys/devices/platform/leds-gpio.1/leds/batt3_led/brightness
        else
            if [ "${n_vlotage}" \> "3662" ]; then
                echo timer > /sys/devices/platform/leds-gpio.1/leds/batt2_led/trigger
                echo 500 > /sys/devices/platform/leds-gpio.1/leds/batt2_led/delay_on
                echo 500 > /sys/devices/platform/leds-gpio.1/leds/batt2_led/delay_off
                sleep 5

                echo none > /sys/devices/platform/leds-gpio.1/leds/batt2_led/trigger
                echo 0 > /sys/devices/platform/leds-gpio.1/leds/batt2_led/brightness
            else
                echo timer > /sys/devices/platform/leds-gpio.1/leds/batt1_led/trigger
                echo 500 > /sys/devices/platform/leds-gpio.1/leds/batt1_led/delay_on
                echo 500 > /sys/devices/platform/leds-gpio.1/leds/batt1_led/delay_off
                sleep 5

                echo none > /sys/devices/platform/leds-gpio.1/leds/batt1_led/trigger
                echo 0 > /sys/devices/platform/leds-gpio.1/leds/batt1_led/brightness
            fi
        fi
    fi
}

main()
{
    echo timer > /sys/devices/platform/leds-gpio.1/leds/syspwr_led/trigger
    echo 1000 > /sys/devices/platform/leds-gpio.1/leds/syspwr_led/delay_on
    echo 1000 > /sys/devices/platform/leds-gpio.1/leds/syspwr_led/delay_off

    echo 0 > /sys/devices/platform/leds-gpio.1/leds/wifi_led/brightness


    batt_led_control
}

if [ "$1" == "press_batt_blink" ]; then
    batt_led_blink
else
    main
fi

