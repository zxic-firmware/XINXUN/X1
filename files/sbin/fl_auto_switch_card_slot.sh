#!/bin/sh

net_conn_flg=0
curr_ping_interval=0

last_recv_byte=0

ping_single_addr()
{
    local pkt_n=1
    local timeout_val=1
    local max_try_cnt=1
    local cnt=0

    while [ $cnt -lt $max_try_cnt ]
    do
        ping -c $pkt_num -W $timeout_v $1
        if [ 0 == $? ]; then
            net_conn_flg=1
            break
        fi

        cnt=`expr $cnt + 1`
        curr_ping_interval=`expr $curr_ping_interval + 1`
    done
}

ping_chk()
{

    local pri_dns_addr=www.ping.cn
    local dns_addr1=www.baidu.com
    local dns_addr2=www.qq.com
    local dns_addr3=8.8.8.8

    net_conn_flg=0
    curr_ping_interval=0

    if [ "${pri_dns_addr}" == "0.0.0.0" ]; then
        pri_dns_addr=14.119.104.189
    fi

    for args in $pri_dns_addr $dns_addr1 $dns_addr2 $dns_addr3
    do
        echo try $args
        ping_single_addr $args

        if [ 1 == $net_conn_flg ]; then
            break
        fi
    done
}

rx_byte_chk()
{
    net_conn_flg=0

    cur_recv_byte=`nv get realtime_rx_bytes`

    if [ "$last_recv_byte" -lt "$cur_recv_byte" ];then
        net_conn_flg=1
        echo "rx_byte_chk saying network works well"
    fi

    last_recv_byte=$cur_recv_byte
}

network_chk()
{
    rx_byte_chk
    if [ "1" == "$net_conn_flg" ]; then
        return
    fi

    ping_chk
}

main()
{
    local curr_state
    local curr_card_slot
    local disconnect_duration=0
    local switch_count=0
    local pkt_num=1
    local timeout_v=1    
    local dst_addr=114.114.114.114    
    local sim_version=`nv get fl_sim_version`
    local sim_switch_count_max
    local ping_interval=`nv get fl_switch_sim_ping_interval`
    local curr_select_mode
    local curr_search_status
    local change_log_name=1
    local curr_dial_mode
    local curr_user_initiate_state
    local manual_dial_flag=0
    local cur_rj45_state
    local cur_conn_mode

    if [ "${sim_version}" == "2" ];then
        sim_switch_count_max=4
    elif [ "${sim_version}" == "3" ];then
        sim_switch_count_max=6
    else
        echo $(date +%Y-%m-%d\ %H:%M:%S) "fl_switch - error: invalid param [${sim_version}]" >> /tmp/test.log
        return
    fi

    echo $(date +%Y-%m-%d\ %H:%M:%S) "fl_switch - begin, sim_version = [${sim_version}]" >> /tmp/test.log
    while [ 1 ]
    do
        curr_state=`nv get ppp_status`

        if [ "${curr_state}" == "ppp_connected" ];then

            network_chk

            if [ 1 == $net_conn_flg ]; then
                nv set fl_used_connected_flag=1
                nv save
                switch_count=0
                change_log_name=1
                disconnect_duration=0

                sleep 60
                continue
            else
                disconnect_duration=`expr $disconnect_duration + $curr_ping_interval`
            fi
        fi

        cur_rj45_state=`nv get rj45_state`
        cur_conn_mode=`nv get blc_wan_mode`

        if [ "-$cur_rj45_state" == "-working" -o "-$cur_conn_mode" == "-PPPOE" ];then
            disconnect_duration=0
            sleep 3
            continue
        fi

        if [ `nv get fl_used_connected_flag` == "0" ];then
            sleep 1
            continue
        fi

        curr_dial_mode=`nv get dial_mode`
        curr_user_initiate_state=`nv get user_initiate_disconnect`

        if [ "${curr_dial_mode}" == "manual_dial" -o "${curr_user_initiate_state}" == "1" ];then
            switch_count=0
            disconnect_duration=0
            if [ "${curr_dial_mode}" == "manual_dial" -a -z "${curr_user_initiate_state}" ];then
                manual_dial_flag=1
            fi
            sleep 5
            continue
        fi

        if [ "${manual_dial_flag}" == "1" ];then
            if [ -n "${curr_user_initiate_state}" -a "${curr_user_initiate_state}" == "0" ];then
                manual_dial_flag=0
            else
                switch_count=0
                disconnect_duration=0
                sleep 5
                continue
            fi
        fi

        if [ "${switch_count}" == "${sim_switch_count_max}" ];then
            if [ "${change_log_name}" == "1" ];then
                echo $(date +%Y-%m-%d\ %H:%M:%S) "fl_switch - switch count [${switch_count}] reach to max [${sim_switch_count_max}]" >> /tmp/test.log
                mv /tmp/test.log /tmp/test1.log
                change_log_name=0
            fi
            sleep 1
            continue
        fi

        disconnect_duration=`expr $disconnect_duration + $ping_interval`

        if [[ "${disconnect_duration}" -ge "60" ]];then
            curr_card_slot=`nv get fl_esim_slot`

            curr_select_mode=`nv get net_select_mode`
            curr_search_status=`nv get manual_search_network_status`
            echo $(date +%Y-%m-%d\ %H:%M:%S) "fl_switch - switch=${disconnect_duration},switch_count=${switch_count} begin switch_slot curr_card_slot=${curr_card_slot} net_select_mode=${curr_select_mode} curr_search_status=${curr_search_status} curr_dial_mode=${curr_dial_mode}" >> /tmp/test.log

            if [ "${curr_select_mode}" == "manual_select" -a "${curr_search_status}" == "searching" ];then
                echo $(date +%Y-%m-%d\ %H:%M:%S) "fl_switch - Manual search process, waiting for the next cut card" >> /tmp/test.log

                disconnect_duration=0
                sleep ${ping_interval}
                continue
            fi

            if [ "${sim_version}" == "3" ];then # 按3卡版本顺序切换
                if [ "${curr_card_slot}" == "1" ];then
                    fl_set_esim_slot set 0 1
                elif [ "${curr_card_slot}" == "0" ];then
                    fl_set_esim_slot set 2 1
                elif [ "${curr_card_slot}" == "2" ];then
                    fl_set_esim_slot set 1 1
                fi
            else                                # 按2卡版本顺序切换
                if [ "${curr_card_slot}" == "1" ];then
                    fl_set_esim_slot set 2 1
                elif [ "${curr_card_slot}" == "2" ];then
                    fl_set_esim_slot set 1 1
                fi
            fi
            disconnect_duration=0
            switch_count=`expr $switch_count + 1`
        fi
        sleep ${ping_interval}
        echo $(date +%Y-%m-%d\ %H:%M:%S) "fl_switch - disconnect_duration=${disconnect_duration},switch_count=${switch_count}" >> /tmp/test.log
    done
}

main
