#!/bin/sh

main() {
    local set_flag=$(nv get fl_set_iptables_flag)
    local lan_ipaddr=$(nv get lan_ipaddr)

    iptables -D INPUT -i br0 -d $lan_ipaddr -p tcp --dport 80 -j DROP
    iptables -D INPUT -i br0 -d $lan_ipaddr -p tcp --dport 443 -j DROP
    iptables -D FORWARD -i br0 -o wan1 -j DROP

    ifconfig br0 up
    sleep 1
    ifconfig usblan0 up
    sleep 1
    ifconfig eth0 up
    sleep 1

    led_ctrl_cmd up_flow
}

main
